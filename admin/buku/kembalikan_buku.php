<?php
$tanggal = date("Y-m-d");
require "inc/koneksi.php";
if (isset($_GET['kode'])) {
    $sql_cek = "SELECT * FROM tb_input WHERE id_buku='" . $_GET['kode'] . "'";
    $query_cek = mysqli_query($koneksi, $sql_cek);
    $data_cek = mysqli_fetch_array($query_cek, MYSQLI_BOTH);
}
?>

<section class="content-header">
    <h1>
        KEMBALIKAN
        <small> Buku</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="index.php">
                <i class="fa fa-home"></i>
                <b>Perpustakaan Digital</b>
            </a>
        </li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">ISI BUKU</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove">
                            <i class="fa fa-remove"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="box-body">

                        <div class="form-group">
                            <label>Id User</label>
                            <input type='text' class="form-control" name="id_user" value="<?php echo $data_cek['id_pengguna']; ?>" readonly />
                        </div>
                        <div class="form-group">
                            <label>Id Buku</label>
                            <input type='text' class="form-control" name="id_buku" value="<?php echo $data_cek['id_buku']; ?>" readonly />
                        </div>
                        <div class="form-group">
                            <label>Nama peminjam</label>
                            <input type='text' class="form-control" name="id_nama" value="<?php echo $data_cek['user_nama']; ?>" readonly />
                        </div>
                        <div class="form-group">
                            <label>Judul Buku</label>
                            <input type='text' class="form-control" name="judul_buku" value="<?php echo $data_cek['judul_buku']; ?>" readonly />
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <input type="submit" name="Ubah" value="Kembalikan" class="btn btn-success">
                        </div>
                </form>
            </div>
            <!-- /.box -->
</section>
<?php


if (isset($_POST['Ubah'])) {
    $user = $_POST['id_user'];
    $buku = $_POST['id_buku'];
    $nama = $_POST['id_nama'];
    $judul = $_POST['judul_buku'];


    $sql_query = "UPDATE `tb_input`,`tb_buku` SET `tb_input`.`status`='tersedia',`tb_buku`.`status`='tersedia' WHERE `tb_input`.`id_buku`=`tb_buku`.`id_buku` AND `tb_buku`.`id_buku`='$buku'";

    $query_simpan = mysqli_query($koneksi, $sql_query);

    if ($query_simpan) {
        echo "<script>
        Swal.fire({title: 'Kembalikan Data Berhasil',text: '',icon: 'success',confirmButtonText: 'OK'
        }).then((result) => {
            if (result.value) {
                window.location = 'page.php';
            }
        })</script>";
    } else {
        echo "<script>
        Swal.fire({title: 'Kembalikan Data Gagal',text: '',icon: 'error',confirmButtonText: 'OK'
        }).then((result) => {
            if (result.value) {
                window.location = 'page.php;
            }
        })</script>";
    }
}
