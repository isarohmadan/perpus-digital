<section class="content-header">
	<h1>
		Koleksi Buku
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="page.php">
				<i class="fa fa-home"></i>
				<b>Perpustakaan Digital</b>
			</a>
		</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<a href="page.php" class="btn btn-primary">
				<i class="fa fa-home"></i>
			</a>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove">
					<i class="fa fa-remove"></i>
				</button>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="table-responsive">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Id Buku</th>
							<th>Judul Buku</th>
							<th>Penerbit</th>
							<th>Read</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>

						<?php
						require 'inc/koneksi.php';
						$hasil = $koneksi->query("SELECT * FROM `tb_input` RIGHT JOIN `tb_buku` ON `tb_buku`.`id_buku` = `tb_input`.`id_buku`;");
						$no = 1;
						$kueri = $hasil->fetch_assoc();
						$tanggal1 = date("Y-m-j");




						if ($_SESSION['ses_id'] == $kueri['id_pengguna'] & $kueri['status'] == "dipinjam") { ?>

							<tr>
								<td>
									<?php echo $no++; ?>
								</td>
								<td>
									<?php echo $kueri['id_buku']; ?>
								</td>
								<td>
									<?php echo $kueri['judul_buku']; ?>
								</td>
								<td>
									<?php echo $kueri['penerbit']; ?>
								</td>

								<td>
									<a href="?page=reading&kode=<?= $kueri['id_buku']; ?>" title="File PDF" class="btn btn-success">
										<i class="fa fa-file"></i> Pakai Buku
									</a>
									<hr>
									<a href="?page=kembalikan_buku&kode=<?= $kueri['id_buku']; ?>" title="File PDF" class="btn btn-primary">
										<i class="fa fa-book"></i>Kembalikan
									</a>

								</td>
								<td>
									<?php echo $kueri['status']; ?>
									<?php if ($kueri['status'] == "dipinjam") { ?>
										<p>
											<hr> Tanggal dikembalikan
											<hr> <?php echo $kueri['tanggal'];
													if (strtotime($kueri['tanggal']) > strtotime($tanggal1)) {
														$hasil2 = (strtotime($kueri['tanggal']) - strtotime($tanggal1)) / 86400;
														echo "<hr>";
														echo $hasil2 . " Hari lagi di kembalikan";
													} elseif (strtotime($kueri['tanggal']) < strtotime($tanggal1)) {
														$hasil = (strtotime($tanggal1) - strtotime($kueri['tanggal'])) / 86400;
														$hasilnya = $hasil * 1000;

													?>
												<hr>
										<h6 class=" alert-danger inline	">KEMBALIKAN BUKU!! </h6>
										<p>jumlah denda = <?= $hasilnya ?></p>
									<?php } ?>
									</p>
								<?php } ?>
								</td>
							</tr>


							<?php
						} else {

							$sql = $koneksi->query("SELECT * from tb_buku WHERE status='tersedia'");
							while ($data = $sql->fetch_assoc()) {
							?>

								<tr>
									<td>
										<?php echo $no++; ?>
									</td>
									<td>
										<?php echo $data['id_buku']; ?>
									</td>
									<td>
										<?php echo $data['judul_buku']; ?>
									</td>
									<td>
										<?php echo $data['penerbit']; ?>
									</td>

									<td>
										<a href="?page=input_data&kode=<?= $data['id_buku']; ?>" title="File PDF" class="btn btn-danger">
											<i class="fa fa-file"></i> Pinjam Buku
										</a>


									</td>
									<td>
										<?php echo $data['status']; ?>
									</td>
								</tr>
						<?php
							}
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>